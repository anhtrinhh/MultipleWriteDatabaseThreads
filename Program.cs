using Confluent.Kafka;
using MultipleWriteDatabaseThreads.Configs;
using MultipleWriteDatabaseThreads.DataAccessLayer;
using MultipleWriteDatabaseThreads.Disruptors;
using MultipleWriteDatabaseThreads.Extensions;
using MultipleWriteDatabaseThreads.Utils;

var builder = WebApplication.CreateBuilder(args);

var appSettings = AppSettings.MapValue(builder.Configuration);
var producerConfig = new ProducerConfig
{
    BootstrapServers = appSettings.BootstrapServers,
    QueueBufferingMaxMessages = 2000000,
    MessageSendMaxRetries = 3,
    RetryBackoffMs = 500,
    LingerMs = 5,
    DeliveryReportFields = "none"
};
var consumerConfig = new ConsumerConfig
{
    BootstrapServers = appSettings.BootstrapServers,
    GroupId = appSettings.ConsumerGroupId,
    SessionTimeoutMs = 6000,
    QueuedMinMessages = 1000000,
    EnableAutoCommit = false,
    EnableAutoOffsetStore = false
};
var producerStringString = new ProducerBuilder<string, string>(producerConfig).Build();
var consumerStringString = new ConsumerBuilder<string, string>(consumerConfig).Build();

builder.Services.AddControllers();
builder.Services.AddRetryPolicy();
builder.Services.AddOracle<TestDbContext>(appSettings.OracleConnectionString);
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSingleton(appSettings);
builder.Services.AddSingleton(new KafkaProducer<string, string>(producerStringString));
builder.Services.AddSingleton(new KafkaConsumer<string, string>(consumerStringString));
builder.Services.AddSingleton(sp =>
{
    const int RingSize = 131072;
    const int SerializeHandlerCount = 1;
    var kafkaProducer = new KafkaProducer<string, string>(producerStringString);
    var disruptor = new Disruptor.Dsl.Disruptor<KafkaMessageBodyEvent>(() => new KafkaMessageBodyEvent(), RingSize);
    var serializeHandlers = new KafkaMessageBodySerializeHandler[SerializeHandlerCount];
    for (var i = 0; i < SerializeHandlerCount; i++)
    {
        serializeHandlers[i] = new KafkaMessageBodySerializeHandler(i + 1);
    }
    disruptor.HandleEventsWith(serializeHandlers)
    .Then(new KafkaMessageBodyProduceHandler(kafkaProducer, appSettings.CommandTopic));
    return disruptor.Start();
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.MapControllers();

app.StartConsumeAndWriteToDb(app.Services, appSettings);

app.Run();
