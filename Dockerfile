FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
ENV http_proxy=http://proxy.fpts.com.vn:8080
ENV https_proxy=http://proxy.fpts.com.vn:8080
WORKDIR /src
COPY "MultipleWriteDatabaseThreads.csproj" "MultipleWriteDatabaseThreads.csproj"
RUN dotnet restore -r linux-musl-x64 "MultipleWriteDatabaseThreads.csproj"
COPY . .
RUN dotnet publish -r linux-musl-x64 -c Release -o /app --self-contained false --no-restore 
FROM dogcer/aspnet-runtime:6.0-alpine-amd64
WORKDIR /app
COPY --from=build /app .
ENV ASPNETCORE_URLS="http://0.0.0.0:80/"
EXPOSE 80
ENTRYPOINT ["dotnet", "MultipleWriteDatabaseThreads.dll"]