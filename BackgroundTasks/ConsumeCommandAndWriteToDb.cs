using Confluent.Kafka;
using Disruptor;
using MultipleWriteDatabaseThreads.Disruptors;

namespace MultipleWriteDatabaseThreads.BackgroundTasks;

public class ConsumeCommandAndWriteToDb
{
    private const int InitialHandlerId = 1;
    private readonly RingBuffer<DisruptorEvent> _ringBuffer;
    private readonly int _handlerCount;
    private Dictionary<string, int> _handlerIdStore;
    private int _currentHandlerId;
    private int _counter;

    public ConsumeCommandAndWriteToDb(RingBuffer<DisruptorEvent> ringBuffer, int handlerCount)
    {
        _ringBuffer = ringBuffer;
        _handlerCount = handlerCount;
        _currentHandlerId = InitialHandlerId;
        _handlerIdStore = new Dictionary<string, int>();
        _counter = 0;
    }
    public void Callback(ConsumeResult<string, string> consumeResult)
    {
        string key = consumeResult.Message.Key;
        if (!_handlerIdStore.ContainsKey(key))
            _handlerIdStore.Add(key, _currentHandlerId++);
        long sequence = _ringBuffer.Next();
        try
        {
            var data = _ringBuffer[sequence];
            data.IsBegin = _counter == 0;
            data.IsEnd = _counter == 299999;
            data.HandlerId = _handlerIdStore[key];
            data.KafkaMessageBodyJson = consumeResult.Message.Value;
        }
        finally
        {
            _ringBuffer.Publish(sequence);
            if (_currentHandlerId > _handlerCount)
                _currentHandlerId = InitialHandlerId;
            if (_counter == 0)
                Console.WriteLine($"Start consume at {DateTime.Now.ToString("hh:mm:ss.fffff")}");
            _counter++;
        }
    }
}