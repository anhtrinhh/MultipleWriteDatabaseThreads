﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MultipleWriteDatabaseThreads.DataAccessLayer.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "USER01");

            migrationBuilder.CreateTable(
                name: "ENTITYAS",
                schema: "USER01",
                columns: table => new
                {
                    Id = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: false),
                    Field1 = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    Field2 = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    Field3 = table.Column<decimal>(type: "DECIMAL(18,2)", nullable: false),
                    Field4 = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: false),
                    Field5 = table.Column<long>(type: "NUMBER(19)", nullable: false),
                    Field6 = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    Field7 = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    Field8 = table.Column<long>(type: "NUMBER(19)", nullable: false),
                    Field9 = table.Column<decimal>(type: "DECIMAL(18,2)", nullable: false),
                    Field10 = table.Column<decimal>(type: "DECIMAL(18,2)", nullable: false),
                    Field11 = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    Field12 = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    Field13 = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: false),
                    Field14 = table.Column<int>(type: "NUMBER(10)", nullable: false),
                    Field15 = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    Field16 = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    Field17 = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    Field18 = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: false),
                    Field19 = table.Column<decimal>(type: "DECIMAL(18,2)", nullable: false),
                    Field20 = table.Column<int>(type: "NUMBER(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ENTITYAS", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ENTITYAS",
                schema: "USER01");
        }
    }
}
