﻿namespace MultipleWriteDatabaseThreads.DataAccessLayer.Entities;
public class EntityA
{
    public string Id { get; set; }
    public string Field1 { get; set; }
    public string Field2 { get; set; }
    public decimal Field3 { get; set; }
    public DateTime Field4 { get; set; }
    public long Field5 { get; set; }
    public string Field6 { get; set; }
    public string Field7 { get; set; }
    public long Field8 { get; set; }
    public decimal Field9 { get; set; }
    public decimal Field10 { get; set; }
    public string Field11 { get; set; }
    public string Field12 { get; set; }
    public DateTime Field13 { get; set; }
    public int Field14 { get; set; }
    public string Field15 { get; set; }
    public string Field16 { get; set; }
    public string Field17 { get; set; }
    public DateTime Field18 { get; set; }
    public double Field19 { get; set; }
    public int Field20 { get; set; }
}
