﻿using Microsoft.EntityFrameworkCore;
using MultipleWriteDatabaseThreads.DataAccessLayer.Entities;

namespace MultipleWriteDatabaseThreads.DataAccessLayer;
public class TestDbContext : DbContext
{
    public TestDbContext(DbContextOptions<TestDbContext> options)
        : base(options)
    {
    }
    public DbSet<EntityA> EntityAs { get; set; }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.HasDefaultSchema("USER01");

        modelBuilder.Entity<EntityA>(entity =>
        {
            entity.HasKey(e => e.Id);

            entity.ToTable("ENTITYAS");

            entity.Property(e => e.Id)
            .HasMaxLength(100);

            entity.Property(e => e.Field1)
            .IsRequired();

            entity.Property(e => e.Field10)
            .HasColumnType("DECIMAL(18,2)");

            entity.Property(e => e.Field11)
            .IsRequired();

            entity.Property(e => e.Field12)
            .IsRequired();

            entity.Property(e => e.Field2)
            .IsRequired();

            entity.Property(e => e.Field3)
            .HasColumnType("DECIMAL(18,2)");

            entity.Property(e => e.Field6)
            .IsRequired();

            entity.Property(e => e.Field7)
            .IsRequired();

            entity.Property(e => e.Field9)
            .HasColumnType("DECIMAL(18,2)");

            entity.Property(e => e.Field15)
            .IsRequired();

            entity.Property(e => e.Field16)
            .IsRequired();

            entity.Property(e => e.Field17)
            .IsRequired();

            entity.Property(e => e.Field19)
            .HasColumnType("DECIMAL(18,2)");
        });
    }
}