using Polly.Retry;

namespace MultipleWriteDatabaseThreads.Factories;

public interface IRetryPolicyFactory
{
    AsyncRetryPolicy CreateAsyncRetryPolicy<TException>(int retries, string prefix = nameof(CreateAsyncRetryPolicy))
    where TException : Exception;
    RetryPolicy CreateRetryPolicy<TException>(int retries, string prefix = nameof(CreateRetryPolicy))
    where TException : Exception;
    AsyncRetryPolicy CreateWaitAsyncRetryPolicy<TException>(int retries, int milliseconds, string prefix = nameof(CreateWaitAsyncRetryPolicy))
    where TException : Exception;
    RetryPolicy CreateWaitRetryPolicy<TException>(int retries, int milliseconds, string prefix = nameof(CreateWaitRetryPolicy))
    where TException : Exception;
    AsyncRetryPolicy CreateWaitForeverAsyncRetryPolicy<TException>(int milliseconds, string prefix = nameof(CreateWaitForeverAsyncRetryPolicy))
    where TException : Exception;
    RetryPolicy CreateWaitForeverRetryPolicy<TException>(int milliseconds, string prefix = nameof(CreateWaitForeverRetryPolicy))
    where TException : Exception;
    AsyncRetryPolicy CreateForeverAsyncRetryPolicy<TException>(string prefix = nameof(CreateForeverAsyncRetryPolicy))
    where TException : Exception;
    RetryPolicy CreateForeverRetryPolicy<TException>(string prefix = nameof(CreateForeverRetryPolicy))
    where TException : Exception;
}