using Polly;
using Polly.Retry;

namespace MultipleWriteDatabaseThreads.Factories;

public class PollyRetryPolicyFactory : IRetryPolicyFactory
{
    private readonly ILogger<PollyRetryPolicyFactory> _logger;

    public PollyRetryPolicyFactory(ILogger<PollyRetryPolicyFactory> logger)
    {
        _logger = logger;
    }
    public AsyncRetryPolicy CreateAsyncRetryPolicy<TException>(int retries, string prefix = nameof(CreateAsyncRetryPolicy))
    where TException : Exception
    {
        return Policy.Handle<TException>().RetryAsync(retries, (ex, retry)
        => LogWarning(ex, retry, retries, prefix));
    }

    public RetryPolicy CreateRetryPolicy<TException>(int retries, string prefix = nameof(CreateRetryPolicy))
    where TException : Exception
    {
        return Policy.Handle<TException>().Retry(retries, (ex, retry)
        => LogWarning(ex, retry, retries, prefix));
    }

    public AsyncRetryPolicy CreateWaitAsyncRetryPolicy<TException>(int retries, int milliseconds, string prefix = nameof(CreateWaitAsyncRetryPolicy))
    where TException : Exception
    {
        return Policy.Handle<TException>().WaitAndRetryAsync(retries, attempt => TimeSpan.FromMilliseconds(milliseconds),
        (ex, timeSpan, retry, ctx) => LogWarning(ex, retry, retries, prefix));
    }

    public RetryPolicy CreateWaitRetryPolicy<TException>(int retries, int milliseconds, string prefix = nameof(CreateWaitRetryPolicy))
    where TException : Exception
    {
        return Policy.Handle<TException>().WaitAndRetry(retries, attempt => TimeSpan.FromMilliseconds(milliseconds),
        (ex, timeSpan, retry, ctx) => LogWarning(ex, retry, retries, prefix));
    }

    public AsyncRetryPolicy CreateWaitForeverAsyncRetryPolicy<TException>(int milliseconds, string prefix = nameof(CreateWaitForeverAsyncRetryPolicy))
    where TException : Exception
    {
        return Policy.Handle<TException>().WaitAndRetryForeverAsync((retry, context) => TimeSpan.FromMilliseconds(milliseconds),
        (ex, retry, timeSpan, context) => LogWarning(ex, retry, int.MaxValue, prefix));
    }

    public RetryPolicy CreateWaitForeverRetryPolicy<TException>(int milliseconds, string prefix = nameof(CreateWaitForeverRetryPolicy))
    where TException : Exception
    {
        return Policy.Handle<TException>().WaitAndRetryForever((retry, context) => TimeSpan.FromMilliseconds(milliseconds),
        (ex, retry, timeSpan, context) => LogWarning(ex, retry, int.MaxValue, prefix));
    }

    public AsyncRetryPolicy CreateForeverAsyncRetryPolicy<TException>(string prefix = nameof(CreateForeverAsyncRetryPolicy))
    where TException : Exception
    {
        return Policy.Handle<TException>().RetryForeverAsync((ex, retry, ctx) => LogWarning(ex, retry, int.MaxValue, prefix));
    }

    public RetryPolicy CreateForeverRetryPolicy<TException>(string prefix = nameof(CreateForeverRetryPolicy))
    where TException : Exception
    {
        return Policy.Handle<TException>().RetryForever((ex, retry, ctx) => LogWarning(ex, retry, int.MaxValue, prefix));
    }

    private void LogWarning(Exception ex, int retry, int retries, string prefix)
    {
        _logger.LogWarning(ex, "[{prefix}] Exception {ExceptionType} with message {Message} detected on attempt {retry} of {retries}",
                        prefix, ex.GetType().Name, ex.Message, retry, retries);
    }
}