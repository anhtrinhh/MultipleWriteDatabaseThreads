using Confluent.Kafka;

namespace MultipleWriteDatabaseThreads.Utils;

public class KafkaProducer<TKey, TValue> : IDisposable
{
    private readonly IProducer<TKey, TValue> _producer;

    public KafkaProducer(IProducer<TKey, TValue> producer)
    {
        _producer = producer;
    }

    public void Produce(Message<TKey, TValue> message, string topic, int partiton = -1)
    {
        try
        {
            if (partiton < 0)
                _producer.Produce(topic, message);
            else
                _producer.Produce(new TopicPartition(topic, partiton), message);
        }
        catch (ProduceException<Null, string> e)
        {
            if (e.Error.Code == ErrorCode.Local_QueueFull)
                _producer.Poll(TimeSpan.FromSeconds(1));
            else
                throw;
        }
    }

    public void Dispose()
    {
        _producer.Dispose();
    }
}