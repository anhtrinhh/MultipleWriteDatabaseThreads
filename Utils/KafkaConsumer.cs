using Confluent.Kafka;

namespace MultipleWriteDatabaseThreads.Utils;

public class KafkaConsumer<TKey, TValue> : IDisposable
{
    private readonly IConsumer<TKey, TValue> _consumer;
    private const int ConsumeTimeoutSecond = 1;

    public KafkaConsumer(IConsumer<TKey, TValue> consumer)
    {
        _consumer = consumer;
    }

    public void Consume(Action<ConsumeResult<TKey, TValue>> callback, string topic, int partition = -1, long offset = -1, CancellationToken cancellationToken = default)
    {
        TimeSpan consumeTimeOut = TimeSpan.FromSeconds(ConsumeTimeoutSecond);
        SubscribeOrAssign(topic, partition, offset);
        while (!cancellationToken.IsCancellationRequested)
        {
            ConsumeResult<TKey, TValue> result = _consumer.Consume(consumeTimeOut);
            if (result != null)
            {
                callback(result);
            }
        }
    }

    public void Dispose()
    {
        _consumer.Dispose();
    }

    private void SubscribeOrAssign(string topic, int partition, long offset)
    {
        if (partition >= 0)
        {
            if (offset >= 0)
                _consumer.Assign(new TopicPartitionOffset(topic, partition, offset));
            else
                _consumer.Assign(new TopicPartition(topic, partition));
        }
        else
            _consumer.Subscribe(topic);
    }
}