using System.Data;
using System.Reflection;
using System.Transactions;
using Disruptor;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MultipleWriteDatabaseThreads.Configs;
using MultipleWriteDatabaseThreads.DataAccessLayer;
using MultipleWriteDatabaseThreads.DataAccessLayer.Entities;
using MultipleWriteDatabaseThreads.Disruptors;
using Oracle.ManagedDataAccess.Client;

namespace MultipleWriteDatabaseThreads.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TestController : ControllerBase
{
    private readonly TestDbContext _db;
    private readonly RingBuffer<KafkaMessageBodyEvent> _ringBuffer;
    private readonly AppSettings _appSettings;
    private readonly DbContextOptions<TestDbContext> _dbContextOptions;

    public TestController(TestDbContext db, RingBuffer<KafkaMessageBodyEvent> ringBuffer, AppSettings appSettings)
    {
        _db = db;
        _ringBuffer = ringBuffer;
        _appSettings = appSettings;
        _dbContextOptions = new DbContextOptionsBuilder<TestDbContext>()
        .UseOracle(appSettings.OracleConnectionString).Options;
    }

    [HttpGet("efcorebatchinsert/{size:int}")]
    public IActionResult EntityFrameworkBatchInsert(int size)
    {
        if (size <= 0)
            return BadRequest();
        var list = GenerateEntityAs(size);
        TestDbContext context = null;
        var start = DateTime.Now.Ticks;
        try
        {
            context = new TestDbContext(_dbContextOptions);
            context.ChangeTracker.AutoDetectChangesEnabled = false;
            int count = 0;
            foreach (var entity in list)
            {
                ++count;
                context = AddToContext(context, entity, count, 100, true);
            }
            context.SaveChanges();
        }
        finally
        {
            if (context != null)
                context.Dispose();
        }
        var duration = DateTime.Now.Ticks - start;
        return Ok($"Write {size} records to db in {duration/10000:F0} ms");

    }

    private TestDbContext AddToContext(TestDbContext context, EntityA entity, int count, int commitCount, bool recreateContext)
    {
        context.Set<EntityA>().Add(entity);
        if (count % commitCount == 0)
        {
            context.SaveChanges();
            if (recreateContext)
            {
                context.Dispose();
                context = new TestDbContext(_dbContextOptions);
                context.ChangeTracker.AutoDetectChangesEnabled = false;
            }
        }
        return context;
    }

    [HttpGet("efinsert/{size:int}")]
    public async Task<IActionResult> EntityFrameworkInsert(int size, string id)
    {
        if (size <= 0)
            return BadRequest();
        var list = GenerateEntityAs(size, id);
        using var transaction = _db.Database.BeginTransaction();
        try
        {
            await _db.EntityAs.AddRangeAsync(list);
            await _db.SaveChangesAsync();
            await transaction.CommitAsync();
            return Ok("Insert Ok");
        }
        catch (DbUpdateException ex)
        {
            HandleException(ex);
            return BadRequest();
        }

    }

    private void HandleException(DbUpdateException exception)
    {
        if (exception.InnerException != null)
        {
            if (exception.InnerException is OracleException oracleException && oracleException.Number == 1)
            {
                Console.WriteLine("============================== Error Unique Contraint ==============================");
            }
        }
    }

    [HttpGet]
    public IActionResult ExecuteRaw()
    {
        _db.Database.ExecuteSqlRaw(@"INSERT INTO EntityAs(Id,Field1,Field2,Field3,Field4,Field5,Field6,
        Field7,Field8,Field9,Field10,Field11,Field12,Field13,Field14) VALUES('Id1','Field1','Field2',13.5,'2020-01-01',25,
        'Field6','Field7',58999,58952.54,4999.69,'Field11','Field12','2022-01-01',998),('Id2','Field1','Field2',13.5,'2020-01-01',25,'Field6','Field7',58999,58952.54,4999.69,'Field11','Field12','2022-01-01',998) UPDATE EntityAs SET Field1='Field1 Value',Field14=28876,Field8=999999 WHERE Id='Id1' UPDATE EntityAs SET Field1='Id2 Field1 Value',Field14=11111,Field8=222222 WHERE Id='Id2' DELETE FROM EntityAs WHERE Id='Id1' OR Id='Id2'");
        return Ok();
    }

    [HttpGet("produceinsert/{size:int}")]
    public IActionResult ProduceInsert(int size)
    {
        if (size <= 0)
            return BadRequest();
        var list = GenerateInsertData(size);
        foreach (var message in list)
        {
            var sequence = _ringBuffer.Next();
            try
            {
                var data = _ringBuffer[sequence];
                data.HandlerId = message.HandlerId;
                data.KafkaMessageBody = new KafkaMessageBody();
                data.KafkaMessageBody.Type = message.KafkaMessageBody.Type;
                data.KafkaMessageBody.EntityA = ReplicaEntity(message.KafkaMessageBody.EntityA);
                data.IsBegin = message.IsBegin;
                data.IsEnd = message.IsEnd;
            }
            finally
            {
                _ringBuffer.Publish(sequence);
                if (message.IsBegin)
                    Console.WriteLine($"Start produce at {DateTime.Now.ToString("hh:mm:ss.fffff")}");
            }
        }
        return Ok();
    }

    [HttpGet("produce/{size:int}")]
    public IActionResult Produce(int size)
    {
        if (size <= 0)
            return BadRequest();
        var list = GenerateData(size);
        foreach (var message in list)
        {
            var sequence = _ringBuffer.Next();
            try
            {
                var data = _ringBuffer[sequence];
                data.HandlerId = message.HandlerId;
                data.KafkaMessageBody = new KafkaMessageBody();
                data.KafkaMessageBody.Type = message.KafkaMessageBody.Type;
                data.KafkaMessageBody.EntityA = ReplicaEntity(message.KafkaMessageBody.EntityA);
                data.IsBegin = message.IsBegin;
                data.IsEnd = message.IsEnd;
            }
            finally
            {
                _ringBuffer.Publish(sequence);
                if (message.IsBegin)
                    Console.WriteLine($"Start produce at {DateTime.Now.ToString("hh:mm:ss.fffff")}");
            }
        }
        return Ok();
    }

    private List<EntityA> GenerateEntityAs(int size, string key = null)
    {
        var list = new List<EntityA>();
        for (int i = 0; i < size; i++)
        {
            string id = string.IsNullOrEmpty(key) ? Guid.NewGuid().ToString() : key;
            list.Add(new EntityA
            {
                Id = id,
                Field1 = id,
                Field2 = id,
                Field3 = i,
                Field4 = DateTime.Now,
                Field5 = i,
                Field6 = id,
                Field7 = id,
                Field8 = i,
                Field9 = i,
                Field10 = i,
                Field11 = id,
                Field12 = id,
                Field13 = DateTime.Now,
                Field14 = i,
                Field15 = id,
                Field16 = id,
                Field17 = id,
                Field18 = DateTime.Now,
                Field19 = i,
                Field20 = i
            });
        }
        return list;
    }

    private DataTable ToDataTable<T>(List<T> items)
    {
        DataTable dataTable = new DataTable(typeof(T).Name);
        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name, prop.PropertyType);
        }
        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }
        //put a breakpoint here and check datatable
        return dataTable;
    }

    private List<KafkaMessageBodyEvent> GenerateInsertData(int size)
    {
        var list = new List<KafkaMessageBodyEvent>();
        var currentHandlerId = 1;
        var isStarted = true;
        var isStoped = false;
        var dateNow = DateTime.Now;
        var rand = new Random();
        for (var i = 0; i < size; i++)
        {
            string id = Guid.NewGuid().ToString();
            var e = new EntityA
            {
                Id = id,
                Field1 = id,
                Field2 = id,
                Field3 = i,
                Field4 = dateNow,
                Field5 = i,
                Field6 = id,
                Field7 = id,
                Field8 = i,
                Field9 = i,
                Field10 = i,
                Field11 = id,
                Field12 = id,
                Field13 = dateNow,
                Field14 = i,
                Field15 = id,
                Field16 = id,
                Field17 = id,
                Field18 = DateTime.Now,
                Field19 = i,
                Field20 = i
            };
            var body = new KafkaMessageBody
            {
                EntityA = e,
                Type = ActionType.Insert
            };
            if (i == size - 1)
                isStoped = true;
            var message = new KafkaMessageBodyEvent
            {
                HandlerId = currentHandlerId,
                IsBegin = isStarted,
                IsEnd = isStoped,
                KafkaMessageBody = body
            };
            list.Add(message);
            isStarted = false;
        }
        return list;
    }

    private List<KafkaMessageBodyEvent> GenerateData(int size)
    {
        var list = new List<KafkaMessageBodyEvent>();
        var currentHandlerId = 1;
        var dateNow = DateTime.Now;
        var isStarted = true;
        var isStoped = false;
        long mark = 2;
        EntityA updateE = null;
        EntityA deleteE = null;
        var rand = new Random();
        for (var i = 0; i < size; i++)
        {
            string id = Guid.NewGuid().ToString();
            var e = new EntityA
            {
                Id = id,
                Field1 = id,
                Field2 = id,
                Field3 = i,
                Field4 = dateNow,
                Field5 = i,
                Field6 = id,
                Field7 = id,
                Field8 = i,
                Field9 = i,
                Field10 = i,
                Field11 = id,
                Field12 = id,
                Field13 = dateNow,
                Field14 = i,
                Field15 = id,
                Field16 = id,
                Field17 = id,
                Field18 = DateTime.Now,
                Field19 = i,
                Field20 = i
            };
            var body = new KafkaMessageBody();
            if (mark % (i + 1) == 2)
                updateE = ReplicaEntity(e);
            else if (mark % (i + 1) == 3 && updateE != null)
            {
                body.Type = ActionType.Update;
                e.Id = updateE.Id;
                deleteE = ReplicaEntity(e);
                updateE = null;
            }
            else if (mark % (i + 1) == 9 && deleteE != null)
            {
                body.Type = ActionType.Delete;
                e = ReplicaEntity(deleteE);
                deleteE = null;
            }
            if (i == size - 1)
                isStoped = true;
            body.EntityA = ReplicaEntity(e);
            // if (body.Type == 1 || body.Type == 2)
            //     Console.WriteLine($"T is {body.Type}, i = {i + 1}, mark = {mark}");
            if (i > mark)
                mark += rand.Next(2, 20);
            var message = new KafkaMessageBodyEvent
            {
                HandlerId = currentHandlerId,
                IsBegin = isStarted,
                IsEnd = isStoped,
                KafkaMessageBody = body
            };
            list.Add(message);
            isStarted = false;
        }
        return list;
    }


    private EntityA ReplicaEntity(EntityA b)
    {
        return new EntityA
        {
            Id = b.Id,
            Field1 = b.Field1,
            Field2 = b.Field2,
            Field3 = b.Field3,
            Field4 = b.Field4,
            Field5 = b.Field5,
            Field6 = b.Field6,
            Field7 = b.Field7,
            Field8 = b.Field8,
            Field9 = b.Field9,
            Field10 = b.Field10,
            Field11 = b.Field11,
            Field12 = b.Field12,
            Field13 = b.Field13,
            Field14 = b.Field14,
            Field15 = b.Field15,
            Field16 = b.Field16,
            Field17 = b.Field17,
            Field18 = b.Field18,
            Field19 = b.Field19,
            Field20 = b.Field20
        };
    }
}