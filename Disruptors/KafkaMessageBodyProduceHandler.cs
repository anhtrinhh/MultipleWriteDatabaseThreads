using Confluent.Kafka;
using Disruptor;
using MultipleWriteDatabaseThreads.Utils;

namespace MultipleWriteDatabaseThreads.Disruptors;

public class KafkaMessageBodyProduceHandler : IEventHandler<KafkaMessageBodyEvent>
{
    private readonly string _topic;
    private readonly KafkaProducer<string, string> _kafkaProducer;

    public KafkaMessageBodyProduceHandler(KafkaProducer<string, string> kafkaProducer, string topic)
    {
        _topic = topic;
        _kafkaProducer = kafkaProducer;
    }
    public void OnEvent(KafkaMessageBodyEvent data, long sequence, bool endOfBatch)
    {
        _kafkaProducer.Produce(new Message<string, string> { Key = data.KafkaMessageBody.EntityA.Id, Value = data.KafkaMessageBodyJson }, _topic);
        if (data.IsEnd)
        {
            Console.WriteLine($"Produce completed at {DateTime.Now.ToString("hh:mm:ss.fffff")}");
        }
    }
}