namespace MultipleWriteDatabaseThreads.Disruptors;

public class KafkaMessageBodyEvent
{
    public int HandlerId { get; set; }
    public int WriteToDbHandlerId { get; set; }
    public string KafkaMessageBodyJson { get; set; }
    public KafkaMessageBody KafkaMessageBody { get; set; }
    public bool IsEnd { get; set; }
    public bool IsBegin { get; set; }
}