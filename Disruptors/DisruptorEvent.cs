namespace MultipleWriteDatabaseThreads.Disruptors;

public class DisruptorEvent
{
    public int HandlerId { get; set; }
    public string KafkaMessageBodyJson { get; set; }
    public bool IsTimeout { get; set; }
    public bool IsEnd { get; set; }
    public bool IsBegin { get; set; }
}