using Disruptor;

namespace MultipleWriteDatabaseThreads.Disruptors;

public class SummaryHandler : IEventHandler<DisruptorEvent>
{
    public void OnEvent(DisruptorEvent data, long sequence, bool endOfBatch)
    {
        if (data.IsEnd)
            Console.WriteLine($"End time is {DateTime.Now.ToString("hh:mm:ss.fffff")}");
    }
}