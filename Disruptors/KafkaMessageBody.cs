using MultipleWriteDatabaseThreads.DataAccessLayer.Entities;

namespace MultipleWriteDatabaseThreads.Disruptors;

public class KafkaMessageBody
{
    public ActionType Type { get; set; }
    public EntityA EntityA { get; set; }
}

public enum ActionType
{
    Insert,
    Update,
    Delete
}