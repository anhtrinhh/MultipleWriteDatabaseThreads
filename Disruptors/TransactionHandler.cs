using System.Text.Json;
using Disruptor;
using Microsoft.EntityFrameworkCore;
using MultipleWriteDatabaseThreads.Configs;
using MultipleWriteDatabaseThreads.DataAccessLayer;
using MultipleWriteDatabaseThreads.DataAccessLayer.Entities;
using MultipleWriteDatabaseThreads.Factories;
using Oracle.ManagedDataAccess.Client;
using Polly.Retry;

namespace MultipleWriteDatabaseThreads.Disruptors;

public class TransactionHandler : IEventHandler<DisruptorEvent>
{
    private const int UniqueConstraintExceptionNumber = 1;
    private const int InitialBatchSize = 0;
    private const string RetryPrefix = "TransactionHandler.ExecuteTransaction";
    private readonly int _handlerId;
    private readonly DbContextOptions<TestDbContext> _dbContextOptions;
    private readonly RetryPolicy _foreverRetryPolicy;
    private readonly int _maxBatchSize;
    private int _currentBatchSize;
    private List<EntityA> _addList;
    private List<EntityA> _updateList;
    private List<EntityA> _deleteList;

    public TransactionHandler(int handlerId, IRetryPolicyFactory retryPolicyFactory, AppSettings appSettings)
    {
        _handlerId = handlerId;
        _currentBatchSize = InitialBatchSize;
        _addList = new List<EntityA>();
        _updateList = new List<EntityA>();
        _deleteList = new List<EntityA>();
        _dbContextOptions = new DbContextOptionsBuilder<TestDbContext>()
        .UseOracle(appSettings.OracleConnectionString).Options;
        _foreverRetryPolicy = retryPolicyFactory.CreateForeverRetryPolicy<Exception>(RetryPrefix);
        _maxBatchSize = appSettings.DisruptorConfig.MaxBatchSize;
    }
    public void OnEvent(DisruptorEvent data, long sequence, bool endOfBatch)
    {
        if (data.HandlerId == _handlerId)
        {
            _currentBatchSize++;
            var message = JsonSerializer.Deserialize<KafkaMessageBody>(data.KafkaMessageBodyJson);
            var entity = message.EntityA;
            var elm = _addList.Where(e => e.Id.Equals(entity.Id)).FirstOrDefault();
            switch (message.Type)
            {
                case ActionType.Insert:
                    if (elm == null)
                        _addList.Add(entity);
                    break;
                case ActionType.Update:
                    if (elm != null)
                        CopyEntity(elm, entity);
                    else
                    {
                        var updateElm = _updateList.Where(e => e.Id.Equals(entity.Id)).FirstOrDefault();
                        if (updateElm != null)
                            CopyEntity(updateElm, entity);
                        else
                            _updateList.Add(entity);
                    }
                    break;
                case ActionType.Delete:
                    if (elm != null)
                        _addList.Remove(elm);
                    else
                    {
                        var uElm = _updateList.Where(e => e.Id.Equals(entity.Id)).FirstOrDefault();
                        if (uElm != null)
                            _updateList.Remove(uElm);
                        _deleteList.Add(entity);
                    }
                    break;
            }
        }
        if ((endOfBatch || _currentBatchSize == _maxBatchSize)
        && (_addList.Count() > 0 || _updateList.Count() > 0 || _deleteList.Count() > 0))
        {
            ExecuteTransaction();
            Reset();
        }
    }

    private void ExecuteTransaction()
    {
        _foreverRetryPolicy.Execute(() =>
        {
            using var context = new TestDbContext(_dbContextOptions);
            using var transaction = context.Database.BeginTransaction();
            try
            {
                if (_addList.Count() > 0)
                {
                    context.EntityAs.AddRange(_addList);
                }
                if (_updateList.Count() > 0)
                {
                    context.EntityAs.UpdateRange(_updateList);
                }
                if (_deleteList.Count() > 0)
                {
                    context.EntityAs.RemoveRange(_deleteList);
                }
                context.SaveChanges();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                if (ex is DbUpdateConcurrencyException concurrencyEx)
                {
                    foreach (var entry in concurrencyEx.Entries)
                    {
                        var entity = (EntityA)entry.Entity;
                        if (_updateList.Contains(entity))
                            _updateList.Remove(entity);
                        if (_deleteList.Contains(entity))
                            _deleteList.Remove(entity);
                    }
                }
                else if (ex is DbUpdateException dbUpdateEx
                && dbUpdateEx.InnerException != null
                && dbUpdateEx.InnerException is OracleException oracleException
                && oracleException.Number == UniqueConstraintExceptionNumber)
                {
                    foreach (var entry in dbUpdateEx.Entries)
                    {
                        var entity = (EntityA)entry.Entity;
                        _addList.Remove(entity);
                    }
                }
                throw;
            }
        });
    }

    private void Reset()
    {
        _currentBatchSize = InitialBatchSize;
        _addList.Clear();
        _updateList.Clear();
        _deleteList.Clear();
    }

    private void CopyEntity(EntityA destination, EntityA source)
    {
        destination.Field1 = source.Field1;
        destination.Field2 = source.Field2;
        destination.Field3 = source.Field3;
        destination.Field4 = source.Field4;
        destination.Field5 = source.Field5;
        destination.Field6 = source.Field6;
        destination.Field7 = source.Field7;
        destination.Field8 = source.Field8;
        destination.Field9 = source.Field9;
        destination.Field10 = source.Field10;
        destination.Field11 = source.Field11;
        destination.Field12 = source.Field12;
        destination.Field13 = source.Field13;
        destination.Field14 = source.Field14;
        destination.Field15 = source.Field15;
        destination.Field16 = source.Field16;
        destination.Field17 = source.Field17;
        destination.Field18 = source.Field18;
        destination.Field19 = source.Field19;
        destination.Field20 = source.Field20;
    }
}