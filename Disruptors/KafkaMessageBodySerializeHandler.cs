using System.Text.Json;
using Disruptor;

namespace MultipleWriteDatabaseThreads.Disruptors;

public class KafkaMessageBodySerializeHandler : IEventHandler<KafkaMessageBodyEvent>
{
    private readonly int _handlerId;

    public KafkaMessageBodySerializeHandler(int handlerId)
    {
        _handlerId = handlerId;
    }
    public void OnEvent(KafkaMessageBodyEvent data, long sequence, bool endOfBatch)
    {
        if (_handlerId == data.HandlerId)
        {
            data.KafkaMessageBodyJson = JsonSerializer.Serialize(data.KafkaMessageBody);
        }
    }
}