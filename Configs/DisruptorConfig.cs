namespace MultipleWriteDatabaseThreads.Configs;

public class DisruptorConfig
{
    public int BufferSize { get; private set; }
    public int NumberOfThreads { get; private set; }
    public int MaxBatchSize { get; private set; }

    public static DisruptorConfig MapValue(IConfiguration configuration)
    {
        var configSection = configuration.GetSection(nameof(DisruptorConfig));
        var bufferSize = configSection.GetValue<int>(nameof(BufferSize));
        var numberOfThreads = configSection.GetValue<int>(nameof(NumberOfThreads));
        var maxBatchSize = configSection.GetValue<int>(nameof(MaxBatchSize));
        if (!IsPowerOf2(bufferSize))
            throw new ArgumentException("BufferSize must be a power of 2");
        if (numberOfThreads <= 0)
            throw new ArgumentException("NumberOfThreads must not be less than 0");
        if (maxBatchSize <= 0)
            throw new ArgumentException("MaxBatchSize must not be less than 0");
        return new DisruptorConfig
        {
            BufferSize = bufferSize,
            NumberOfThreads = numberOfThreads,
            MaxBatchSize = maxBatchSize
        };
    }

    private static bool IsPowerOf2(int x)
    {
        return x > 0 && (x & (x - 1)) == 0;
    }
}