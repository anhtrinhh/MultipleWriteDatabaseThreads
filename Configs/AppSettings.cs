namespace MultipleWriteDatabaseThreads.Configs;

public class AppSettings
{
    public string OracleConnectionString { get; private set; }
    public string SqlServerConnectionString { get; private set; }
    public string BootstrapServers { get; private set; }
    public string CommandTopic { get; private set; }
    public string ConsumerGroupId { get; private set; }
    public DisruptorConfig DisruptorConfig { get; private set; }
    public static AppSettings MapValue(IConfiguration configuration)
    {
        string oracleConnectionString = configuration[nameof(OracleConnectionString)];
        string sqlServerConnectionString = configuration[nameof(SqlServerConnectionString)];
        string bootstrapServers = configuration[nameof(BootstrapServers)];
        string commandTopic = configuration[nameof(CommandTopic)];
        string consumerGroupId = configuration[nameof(ConsumerGroupId)];
        DisruptorConfig disruptorConfig = DisruptorConfig.MapValue(configuration);
        if (string.IsNullOrEmpty(oracleConnectionString))
            throw new ArgumentNullException("OracleConnectionString is null");
        if (string.IsNullOrEmpty(sqlServerConnectionString))
            throw new ArgumentNullException("SqlServerConnectionString is null");
        if (string.IsNullOrEmpty(bootstrapServers))
            throw new ArgumentNullException("BootstrapServers is null");
        if (string.IsNullOrEmpty(commandTopic))
            throw new ArgumentNullException("CommandTopic is null");
        if (string.IsNullOrEmpty(consumerGroupId))
            throw new ArgumentNullException("ConsumerGroupId is null");
        return new AppSettings
        {
            OracleConnectionString = oracleConnectionString,
            SqlServerConnectionString = sqlServerConnectionString,
            BootstrapServers = bootstrapServers,
            CommandTopic = commandTopic,
            ConsumerGroupId = consumerGroupId,
            DisruptorConfig = disruptorConfig
        };
    }
}