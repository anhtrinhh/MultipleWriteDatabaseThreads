using MultipleWriteDatabaseThreads.BackgroundTasks;
using MultipleWriteDatabaseThreads.Configs;
using MultipleWriteDatabaseThreads.Disruptors;
using MultipleWriteDatabaseThreads.Factories;
using MultipleWriteDatabaseThreads.Utils;

namespace MultipleWriteDatabaseThreads.Extensions;

public static class IHostExtensions
{
    public static void StartConsumeAndWriteToDb(this IHost host, IServiceProvider services, AppSettings appSettings)
    {
        var kafkaConsumer = services.GetRequiredService<KafkaConsumer<string, string>>();
        var retryPolicyFactory = services.GetRequiredService<IRetryPolicyFactory>();
        var hanlders = GetHandlers(appSettings.DisruptorConfig.NumberOfThreads, retryPolicyFactory, appSettings);
        var disruptor = new Disruptor.Dsl.Disruptor<DisruptorEvent>(() => new DisruptorEvent(), appSettings.DisruptorConfig.BufferSize);
        disruptor.HandleEventsWith(hanlders)
        .Then(new SummaryHandler());
        var backgroundTask = new ConsumeCommandAndWriteToDb(disruptor.Start(), appSettings.DisruptorConfig.NumberOfThreads);
        Task.Factory.StartNew(() => kafkaConsumer.Consume(backgroundTask.Callback, appSettings.CommandTopic, 0, 0, default),
        default, TaskCreationOptions.LongRunning, TaskScheduler.Default);
    }

    private static TransactionHandler[] GetHandlers(int size, IRetryPolicyFactory retryPolicyFactory, AppSettings appSettings)
    {
        var handlers = new TransactionHandler[size];
        for (var i = 0; i < size; i++)
            handlers[i] = new TransactionHandler(i + 1, retryPolicyFactory, appSettings);
        return handlers;
    }

}