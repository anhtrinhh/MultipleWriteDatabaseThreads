using MultipleWriteDatabaseThreads.Factories;

namespace MultipleWriteDatabaseThreads.Extensions;

public static class IServiceCollectionExtensions
{
    public static IServiceCollection AddRetryPolicy(this IServiceCollection services)
    => services.AddSingleton<IRetryPolicyFactory, PollyRetryPolicyFactory>();
}